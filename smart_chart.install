<?php

/**
 * @file
 * Installation file for Smart Chart module.
 */

/**
 * Implements hook_install().
 */
function smart_chart_install() {
  $t = get_t();

  // Chart content type.
  $content_type = array(
    'type' => 'smart_chart',
    'name' => 'Smart Chart',
    'description' => 'Chart that displays nodes in tree view.',
    'title_label' => 'Title',
    'base' => 'node_content',
    'custom' => TRUE,
  );

  // Set remaining definitions with defaults.
  $node_type = node_type_set_defaults($content_type);

  // Save content type.
  node_type_save($node_type);
  node_add_body_field($node_type, 'Description');

  // Set node options.
  variable_set('node_preview_smart_chart', 0);
  variable_set('node_options_smart_chart', array('status'));
  variable_set('node_submitted_smart_chart', 0);
  variable_set('menu_options_smart_chart', array(''));
  variable_set('menu_parent_smart_chart', ':0');

  // Connection content type.
  $content_type = array(
    'type' => 'smart_connection',
    'name' => 'Smart Connection',
    'description' => 'Connection content type that connects items in a chart.',
    'title_label' => 'Title',
    'base' => 'node_content',
    'custom' => TRUE,
  );

  // Set remaining definitions with defaults.
  $node_type = node_type_set_defaults($content_type);

  // Save content type.
  node_type_save($node_type);

  // Set node options.
  variable_set('node_preview_smart_connection', 0);
  variable_set('node_options_smart_connection', array('status'));
  variable_set('node_submitted_smart_connection', 0);
  variable_set('menu_options_smart_connection', array(''));
  variable_set('menu_parent_smart_connection', ':0');

  // Create Smart Chart field.
  $field = array(
    'field_name' => 'field_sc_chart',
    'type' => 'entityreference',
    'cardinality' => 1,
    'settings' => array(
      'target_type' => 'node',
      'handler_settings' => array('target_bundles' => array('smart_chart')),
    ),
  );
  field_create_field($field);

  // Create Smart Chart field instance.
  $instance = array(
    'field_name' => 'field_sc_chart',
    'entity_type' => 'node',
    'bundle' => 'smart_connection',
    'label' => 'Smart Chart',
    'widget' => array(
      'type' => 'entityreference_autocomplete',
    ),
    'settings' => array(
      'target_type' => 'node',
      'handler_settings' => array('target_bundles' => array('smart_chart')),
    ),
  );
  field_create_instance($instance);

  // Create parent field.
  $field = array(
    'field_name' => 'field_sc_parent',
    'type' => 'entityreference',
    'cardinality' => 1,
    'settings' => array(
      'target_type' => 'node',
      'handler_settings' => array('target_bundles' => array('smart_chart')),
    ),
  );
  field_create_field($field);

  // Create parent field instance.
  $instance = array(
    'field_name' => 'field_sc_parent',
    'entity_type' => 'node',
    'bundle' => 'smart_connection',
    'label' => 'Parent',
    'widget' => array(
      'type' => 'entityreference_autocomplete',
    ),
    'settings' => array(
      'target_type' => 'node',
      'handler_settings' => array('target_bundles' => array('smart_chart')),
    ),
  );
  field_create_instance($instance);

  // Create child field.
  $field = array(
    'field_name' => 'field_sc_child',
    'type' => 'entityreference',
    'cardinality' => 1,
    'settings' => array(
      'target_type' => 'node',
    ),
    'settings' => array(
      'target_type' => 'node',
      'handler_settings' => array('target_bundles' => array()),
    ),
  );
  field_create_field($field);

  // Create child field instance.
  $instance = array(
    'field_name' => 'field_sc_child',
    'entity_type' => 'node',
    'bundle' => 'smart_connection',
    'label' => 'Child',
    'widget' => array(
      'type' => 'entityreference_autocomplete',
    ),
    'settings' => array(
      'target_type' => 'node',
      'handler_settings' => array('target_bundles' => array()),
    ),
  );
  field_create_instance($instance);

  // Load taxonomy module.
  module_load_include('module', 'taxonomy', 'taxonomy');

  // Create Smart Chart Type vocabulary.
  $vocabulary = (object) array(
    'name' => 'Chart Type',
    'machine_name' => 'smart_chart_type',
    'description' => '',
    'hierarchy' => 0,
    'module' => 'taxonomy',
    'weight' => 0,
    'language' => 'und',
    'i18n_mode' => 1,
  );
  taxonomy_vocabulary_save($vocabulary);

  module_load_include('inc', 'smart_chart', 'includes/smart_chart.functions');
  // Create taxonomy terms.
  $term = array(
    'name' => 'Personnel',
    'description' => 'Personnel chart allows users to diagram reporting
      relationships in their company. It is required in order for permissions to
      work and can not be deleted.',
    'machine_name' => 'personnel',
  );
  sc_add_chart_type($term);

  $term = array(
    'name' => 'Corporate',
    'description' => 'Corporations can have many organization structures, but
      the most typical corporation organizational structure consists of the
      shareholder, board of directors, the officers and employees.',
    'machine_name' => 'corporate',
  );
  sc_add_chart_type($term);

  $term = array(
    'name' => 'Regional',
    'description' => 'Regional smart chart.',
    'machine_name' => 'regional',
  );
  sc_add_chart_type($term);

  // Add term reference to smart chart.
  $field = array(
    'field_name' => 'field_sc_type',
    'type' => 'taxonomy_term_reference',
    'settings' => array(
      'allowed_values' => array(
        array(
          'vocabulary' => $vocabulary->machine_name,
          'parent' => 0,
        ),
      ),
    ),
  );
  field_create_field($field);

  $instance = array(
    'field_name' => 'field_sc_type',
    'entity_type' => 'node',
    'label' => 'Chart type',
    'bundle' => 'smart_chart',
    'required' => TRUE,
    'widget' => array(
      'type' => 'options_select',
    ),
    'display' => array(
      'default' => array('type' => 'hidden'),
      'teaser' => array('type' => 'hidden'),
    ),
  );
  field_create_instance($instance);
}

/**
 * Implements hook_uninstall().
 */
function smart_chart_uninstall() {
  $t = get_t();

  $types = array('smart_chart', 'smart_connection');

  // Get all nids with group content Type.
  $result = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('type', $types, 'IN')
    ->execute()
    ->fetchAll();

  $nids = array();
  foreach ($result as $row) {
    $nids[] = $row->nid;
  }

  // Delete content.
  node_delete_multiple($nids);
  drupal_set_message($t('All nodes of content type were deleted.'));

  // Remove Content Type.
  node_type_delete('smart_chart');
  node_type_delete('smart_connection');
  drupal_set_message($t('Content type was deleted.'));

  // Delete vocabulary Smart Chart Type.
  $vocabulary = taxonomy_vocabulary_machine_name_load('smart_chart_type');
  taxonomy_vocabulary_delete($vocabulary->vid);

  // Clean removed fields.
  field_purge_batch(1000);
}
