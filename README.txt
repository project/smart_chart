README.txt
==========

A content type that implements a tree view chart. It can use different
content types for its items, that can be selected through the admin interface.

The Chart is viewable through "node/%nid/chart" as a tab or stand alone on
"chart/%nid".

The first thing you have to do is to go to
"admin/config/content/smart-suite/chart" and add content types that
you want as chart items.

100% ajax based, unless you add/remove the first item. I am working on it.

!Important: You need to enable at least JQuery 1.7 version.

AUTHOR/MAINTAINER
======================
Author: Thanos Nokas(Matrixlord)
Maintainer:  Thanos Nokas(Matrixlord) (https://drupal.org/user/1538394)
